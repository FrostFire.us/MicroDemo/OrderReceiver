﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrostFire.Models;
using FrostFire.OrderReceiver.Helpers;
using FrostFire.OrderReceiver.Interfaces;
using FrostFire.OrderReceiver.Models;
using FrostFire.OrderReceiver.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace FrostFire.OrderReceiver
{
    public class Startup
    {
        private readonly IConfigurationRoot _configuration;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            _configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.OutputFormatters.Add(new XmlSerializerOutputFormatter());
            });

            services.AddRedis(cfg =>
            {
                cfg.Server = _configuration["MQ:Server"];
                cfg.List = _configuration["MQ:List"];
                cfg.Channel = _configuration["MQ:Channel"];
                cfg.MessageOut = _configuration["MQ:MessageOut"];
            });

            //services.AddHttpsRedirection(cfg =>
            //{
            //    cfg.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
            //    cfg.HttpsPort = 5001;
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Order, OrderDTO>();
            });


            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
