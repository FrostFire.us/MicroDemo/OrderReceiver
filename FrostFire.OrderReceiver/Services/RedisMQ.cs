﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrostFire.OrderReceiver.Interfaces;
using Microsoft.Extensions.Options;
using StackExchange.Redis;


namespace FrostFire.OrderReceiver.Services
{
    public class RedisMQ : IMessageQueue, IDisposable
    {
        private readonly IOptions<RedisConfig> _config;
        private ConnectionMultiplexer _connection;


        public RedisMQ(IOptions<RedisConfig> Config)
        {
            _config = Config;
            Initialize();
        }

        private void Initialize()
        {
            if (_config.Value.Server == null)
                throw new ArgumentNullException(nameof(_config.Value.Server));

            if (_config.Value.List == null)
                throw new ArgumentNullException(nameof(_config.Value.List));

            if (_config.Value.Channel == null)
                throw new ArgumentNullException(nameof(_config.Value.Channel));

            if (_config.Value.MessageOut == null)
                throw new ArgumentNullException(nameof(_config.Value.MessageOut));

            _connection = ConnectionMultiplexer.Connect(_config.Value.Server);
        }

        private IDatabase DB => _connection.GetDatabase();

        public void Submit(string Data)
        {
            if (!_connection.IsConnected)
                Initialize();
            DB.ListLeftPush(_config.Value.List, Data);
        }

        public void SubmitAndNotify(string Data)
        {
            Submit(Data);
            Notify();
        }

        public void Notify()
        {
            if (!_connection.IsConnected)
                Initialize();
            DB.Publish(_config.Value.Channel, _config.Value.MessageOut);
        }

        public void Notify(string Data)
        {
            throw new NotImplementedException();
        }


        public class RedisConfig
        {
            public string Server { get; set; }
            public string List { get; set; }
            public string Channel { get; set; }
            public string MessageOut { get; set; }
        }

        public void Dispose()
        {
            _connection?.Dispose();
        }
    }
}
