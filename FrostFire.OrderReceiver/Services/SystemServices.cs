﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrostFire.OrderReceiver.Services
{
    public static class SystemServices
    {
        public static string GetSystemID()
        {
            return Environment.MachineName;
        }
    }
}
