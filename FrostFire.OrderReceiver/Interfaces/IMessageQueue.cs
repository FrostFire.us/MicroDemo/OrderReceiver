﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrostFire.OrderReceiver.Interfaces
{
    public interface IMessageQueue
    {
        void Submit(string Data);
        void SubmitAndNotify(string Data);
        void Notify();
        void Notify(string Data);
    }
}
