﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrostFire.Models;

namespace FrostFire.OrderReceiver.Models
{
    public class OrderDTO: Order
    {
        public string Receiver { get; set; }
    }
}
