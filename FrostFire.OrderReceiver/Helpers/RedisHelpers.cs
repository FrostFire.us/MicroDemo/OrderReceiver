﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrostFire.OrderReceiver.Interfaces;
using FrostFire.OrderReceiver.Services;
using Microsoft.Extensions.DependencyInjection;

namespace FrostFire.OrderReceiver.Helpers
{
    public static class RedisHelpers
    {
        public static IServiceCollection AddRedis(this IServiceCollection Collection, Action<RedisMQ.RedisConfig> Config)
        {
            if(Collection == null)
                throw new ArgumentNullException();

            if(Config==null)
                throw new ArgumentNullException();

            Collection.Configure(Config);
            return Collection.AddSingleton<IMessageQueue, RedisMQ>();
        }
    }
}
