﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace FrostFire.OrderReceiver.Controllers
{
    [Route("[Controller]")]
    public class SystemController: Controller
    {
        [HttpGet("ping")]
        public IActionResult Ping()
        {
            return Ok($"{Environment.MachineName}:{Request.HttpContext.Connection.LocalIpAddress}");
        }

    }
}
