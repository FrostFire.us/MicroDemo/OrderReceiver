﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FrostFire.Models;
using FrostFire.OrderReceiver.Interfaces;
using FrostFire.OrderReceiver.Models;
using FrostFire.OrderReceiver.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace FrostFire.OrderReceiver.Controllers
{
    [Route("[Controller]")]
    public class OrdersController: Controller
    {
        private readonly IMessageQueue _messageQueue;
        private readonly ILogger<OrdersController> _logger;

        public OrdersController(IMessageQueue MessageQueue, ILogger<OrdersController> Logger)
        {
            _messageQueue = MessageQueue;
            _logger = Logger;
        }

        [HttpPost("Submit")]
        public IActionResult Submit([FromBody] Order Order)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (Order == null)
                return BadRequest();

            _logger.LogInformation($" Received order {Order.OrderID}");

            OrderDTO order= Mapper.Map<OrderDTO>(Order);
            order.Receiver = SystemServices.GetSystemID();
            _messageQueue.SubmitAndNotify(JsonConvert.SerializeObject(order));
           
            return Ok("Order Submitted to queue");
        }
    }
}
